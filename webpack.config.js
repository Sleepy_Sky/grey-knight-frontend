const path = require('path');

module.exports = {
  mode: 'development',
  entry: './app/index.js',
  output: {
    path: path.resolve(__dirname, 'public'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader']
      },
      {
        test:   /\.(eot|woff|woff2|svg|ttf)([\?]?.*)$/,
        loader: 'file-loader'
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        options: {
          presets: ['@babel/preset-env', '@babel/preset-react'],
          plugins: ['@babel/proposal-class-properties']
        }
      }
    ]
  },
  devServer: {
    publicPath: '/',
    contentBase: path.resolve(__dirname, 'public'),
    watchContentBase: true,
    compress: true,
    proxy: {
      '/api': 'http://localhost:8001',
    },
  }
};