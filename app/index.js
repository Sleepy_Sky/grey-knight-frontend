import React from 'react';
import ReactDom from 'react-dom';
import Layout from './components/container';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducers from './reducers';

const store = createStore(reducers);

ReactDom.render(<Provider store={store}><Layout /></Provider>, document.getElementById('app'));
