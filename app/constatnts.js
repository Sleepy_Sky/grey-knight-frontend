export const CHARACTER_SHEET_REQUEST = 'CHARACTER_SHEET_REQUEST';
export const CHARACTER_SHEET_REQUEST_SUCCESS = 'CHARACTER_SHEET_REQUEST_SUCCESS';
export const CHARACTER_SHEET_REQUEST_ERROR = 'CHARACTER_SHEET_REQUEST_ERROR';
export const SELECTED_MENU_TAB = 'SELECTED_MENU_TAB';

export const CHARACTER_CREATOR_REQUEST = 'CHARACTER_CREATOR_REQUEST';
export const CHARACTER_CREATOR_REQUEST_SUCCESS = 'CHARACTER_CREATOR_REQUEST_SUCCESS';
export const CHARACTER_CREATOR_REQUEST_ERROR = 'CHARACTER_CREATOR_REQUEST_ERROR';

export const INITIAL_REQUEST = 'INITIAL_REQUEST';
export const INITIAL_REQUEST_SUCCESS = 'INITIAL_REQUEST_SUCCESS';

export const SELECTED_CONTENT = 'SELECTED_CONTENT';

export const OPEN_MAIN_MENU = 'OPEN_MAIN_MENU';
export const CLOSE_MAIN_MENU = 'CLOSE_MAIN_MENU';

export const OPEN_LOGIN_REGISTER = 'OPEN_LOGIN_REGISTER';
export const CLOSE_LOGIN_REGISTER = 'CLOSE_LOGIN_REGISTER';


