import * as constants from '../../constatnts';

export const openMainMenu = () => ({
  type: constants.OPEN_MAIN_MENU,
  mainMenuToggle: true,
});

export const closeMainMenu = () => ({
  type: constants.CLOSE_MAIN_MENU,
  mainMenuToggle: false,
});

export const openLoginRegister = (event) => {
  const toReturn = event.currentTarget;
  return ({
    type: constants.OPEN_LOGIN_REGISTER,
    loginRegisterToggle: toReturn,
  });
};

export const closeLoginRegister = () => ({
  type: constants.CLOSE_LOGIN_REGISTER,
  loginRegisterToggle: null,
});