import React from 'react';
import { bool, func } from 'prop-types';
import { withStyles, Drawer, List, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import { SupervisorAccount, PersonAdd  } from '@material-ui/icons';
import { SelectionContext } from '../Layout';
const styles = () => ({
  root: {
    flexGrow: 1,
  },
});

const MainMenu = ({ classes, mainMenuToggle, closeMainMenu }) => {
  return (
    <SelectionContext.Consumer>
      {value => {
        const changeContentCloseMenu = (e) => {
          value(e);
          closeMainMenu();
        };
        return (
          <Drawer open={mainMenuToggle} onClose={closeMainMenu}>
            <div className={classes.root}>
              <List component="nav" >
                <ListItem button id="characterSheet" onClick={changeContentCloseMenu}>
                  <ListItemIcon>
                    <SupervisorAccount />
                  </ListItemIcon>
                  <ListItemText primary="Character Sheet" secondary="It will be changed"/>
                </ListItem>
                <ListItem button id="characterCreator" onClick={changeContentCloseMenu}>
                  <ListItemIcon>
                    <PersonAdd />
                  </ListItemIcon>
                  <ListItemText primary="Create New Character"/>
                </ListItem>
              </List>
            </div>
          </Drawer>
        );
      }}
    </SelectionContext.Consumer>

  );
};

MainMenu.propTypes = {
  ...withStyles.propTypes,
  mainMenuToggle: bool.isRequired,
  closeMainMenu: func.isRequired,
};

export default withStyles(styles)(MainMenu);
