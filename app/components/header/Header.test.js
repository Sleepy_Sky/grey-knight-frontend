import React from 'react';
import { shallow } from 'enzyme';

import Header from './container';

describe('<Header />', () => {
  it('should match snapshot',() => {
    const wrapper = shallow(<Header
      mainMenuToggle={false}
      openMainMenu={() => undefined}
      closeMainMenu={() => undefined}/>);

    expect(wrapper).toMatchSnapshot();
  });
});