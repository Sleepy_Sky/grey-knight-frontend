import React, { Component, Fragment } from 'react';
import { bool, func } from 'prop-types';
import { withStyles, AppBar, Toolbar, IconButton, Typography, Popover } from '@material-ui/core';
import { Menu, AccountCircle } from '@material-ui/icons';
import MainMenu from './MainMenu';
import LoginRegisterContainer from './login_register/LoginRegisterContainer';

const styles = () => ({
  headerTitle: {
    flexGrow: 1,
  },
});

class Header extends Component {

  render() {
    const {
      classes,
      //  Main Menu props
      mainMenuToggle,
      openMainMenu,
      closeMainMenu,
      //  Login Register Menu props
      loginRegisterToggle,
      openLoginRegister,
      closeLoginRegister
    } = this.props;


    const loginRegisterBoolToggle = Boolean(loginRegisterToggle);


    return(
      <Fragment>
        <AppBar position="static">
          <Toolbar>
            <IconButton color="inherit" onClick={openMainMenu}>
              <Menu />
            </IconButton>
            <Typography
              className={classes.headerTitle}
              align="center"
              variant="h6"
              color="inherit">
              Grey Knight
            </Typography>
            <IconButton color="inherit" onClick={openLoginRegister}>
              <AccountCircle />
            </IconButton>
          </Toolbar>
        </AppBar>
        <MainMenu mainMenuToggle={mainMenuToggle} closeMainMenu={closeMainMenu} />

        <Popover
          open={loginRegisterBoolToggle}
          anchorEl={loginRegisterToggle}
          onClose={closeLoginRegister}
          anchorOrigin={{
            vertical: 'bottom',
            horizontal: 'right',
          }}
          transformOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}>
          <LoginRegisterContainer />
        </Popover>


      </Fragment>
    );
  }
}

Header.propTypes = {
  ...withStyles.propTypes,
  mainMenuToggle: bool.isRequired,
  openMainMenu: func.isRequired,
  closeMainMenu: func.isRequired,
};

export default withStyles(styles)(Header);