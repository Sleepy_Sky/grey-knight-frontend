import { connect } from 'react-redux';
import { openMainMenu, closeMainMenu, openLoginRegister, closeLoginRegister } from './actions';
import Header from './Header';

const mapStateToProps = state => ({
  mainMenuToggle: state.headerReducer.mainMenuToggle,
  loginRegisterToggle: state.headerReducer.loginRegisterToggle,
});

const mapDispatchToProps = dispatch => ({
  openMainMenu: () => dispatch(openMainMenu()),
  closeMainMenu: () => dispatch(closeMainMenu()),
  openLoginRegister: event => dispatch(openLoginRegister(event)),
  closeLoginRegister: () => dispatch(closeLoginRegister()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Header);