import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import green from '@material-ui/core/colors/green';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  buttonStyle: {
    marginBottom: theme.spacing.unit*2,
    marginTop: theme.spacing.unit*2,
    marginRight: theme.spacing.unit*3,
    marginLeft: theme.spacing.unit*3,
  },
  buttonProgress: {
    position: 'absolute',
    color: green[500],
    top: '50%',
    left: '50%',
    marginTop: -14,
    marginLeft: -12,
  },
});







class RegisterItem extends Component {
  state = {
    registrationData: {},
    passError: false,
    loading: false,
  };

  sendData = (url, data) => {
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    }).then(res => {
      // console.log(res);
      this.setState({
        loading: false,
      });
      return res.json();
    }).then(res => {
      console.log(JSON.parse(res));  // eslint-disable-line no-console
    });
  };

  componentDidUpdate() {
    const { loading, registrationData } = this.state;
    if(loading === true) {
      const url = '/api/newAccount';
      this.sendData(url, registrationData);
    }
  }

  prepareDataToSend = () => {
    const login = document.getElementById('registerLogin').value;
    const pass1 = document.getElementById('registerPass').value;
    const pass2 = document.getElementById('registerPassRep').value;
    if(pass1.length < 2 || pass2.length < 2 || pass1 !== pass2) {
      alert('ERROR, INCORECT PASSWORD!');
    } else {
      this.setState({
        registrationData: {
          login: login,
          password: pass1,
        },
        loading: true,
      });
    }

  };

  checkPassword = () => {
    const pass1 = document.getElementById('registerPass').value;
    const pass2 = document.getElementById('registerPassRep').value;
    if(pass1.length < 2 || pass2.length < 2 || pass1 !== pass2) {
      this.setState({ passError: true });
    }
    else {
      this.setState({ passError: false });
    }
  };

  render() {
    const { classes } = this.props;
    const { passError, loading } = this.state;

    return(
      <Grid container direction="column" spacing={0}>
        <TextField
          id="registerLogin"
          label="Login"
          className={classes.textField}/>
        <TextField
          id="registerPass"
          error={passError}
          label="Password"
          type="password"
          className={classes.textField}
        />
        <TextField
          id="registerPassRep"
          error={passError}
          label="Repeat Password"
          type="password"
          className={classes.textField}
          onChange={this.checkPassword}
        />
        <Button
          onClick={this.prepareDataToSend}
          variant="contained"
          color="secondary"
          className={classes.buttonStyle}>
          REGISTER
          {loading && <CircularProgress
            size={28}
            className={classes.buttonProgress}
            thickness={7}/>}
        </Button>
      </Grid>
    );
  }
}

RegisterItem.propTypes = {
  ...withStyles.propTypes,
};

export default withStyles(styles)(RegisterItem);