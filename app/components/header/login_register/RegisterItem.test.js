import React from 'react';
import { shallow } from 'enzyme';

import RegisterItem from './RegisterItem';

describe('<RegisterItem />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<RegisterItem />);

    expect(wrapper).toMatchSnapshot();
  });
});