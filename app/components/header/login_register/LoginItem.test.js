import React from 'react';
import { shallow } from 'enzyme';

import LoginItem from './LoginItem';

describe('<LoginItem />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<LoginItem />);

    expect(wrapper).toMatchSnapshot();
  });
});