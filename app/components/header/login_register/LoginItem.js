import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  buttonStyle: {
    marginBottom: theme.spacing.unit*2,
    marginTop: theme.spacing.unit*2,
    marginRight: theme.spacing.unit*3,
    marginLeft: theme.spacing.unit*3,
  },
});







class LoginItem extends Component {
  state = {
    userData: {},
    send: false,
  };

  sendData = (url, data) => {
    fetch(url, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: { 'Content-Type': 'application/json' },
    }).then(res => {
      this.setState({ send: false });
      return res.json();
    }).then(res => {
      console.log(JSON.parse(res)); // eslint-disable-line no-console
    });
  };

  componentDidUpdate() {
    const { userData, send } = this.state;
    if(send === true) {
      const url = '/api/logIn';
      this.sendData(url, userData);
    }
  }

  logTheUser = () => {
    const login = document.getElementById('login').value;
    const password = document.getElementById('password').value;
    this.setState({
      userData: {
        login: login,
        password: password,
      },
      send: true,
    });
  };

  render() {
    const { classes } = this.props;
    return(
      <Grid container direction="column" spacing={0}>
        <TextField
          id="login"
          label="Login"
          className={classes.textField}/>
        <TextField
          id="password"
          label="Password"
          type="password"
          className={classes.textField}
        />
        <Button
          onClick={this.logTheUser}
          variant="contained"
          color="secondary"
          className={classes.buttonStyle}>
          LOGIN
        </Button>
      </Grid>
    );
  }
}

LoginItem.propTypes = {
  ...withStyles.propTypes,
};

export default withStyles(styles)(LoginItem);