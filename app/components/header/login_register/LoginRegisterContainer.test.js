import React from 'react';
import { shallow } from 'enzyme';

import LoginRegisterContainer from './LoginRegisterContainer';

describe('<LoginRegisterContainer />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<LoginRegisterContainer />);

    expect(wrapper).toMatchSnapshot();
  });
});