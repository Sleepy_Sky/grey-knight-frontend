import React, { Component } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import CardActions from '@material-ui/core/CardActions';
import Grid from '@material-ui/core/Grid';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';

import LoginItem from './LoginItem';
import RegisterItem from './RegisterItem';


const styles = theme => ({
  itemWidth: {
    width: '250px',
  },
  heading: {
    textAlign: 'center',
    fontSize: theme.typography.pxToRem(18),
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(16),
    color: theme.palette.text.secondary,
  },
  textContainer: {
    display: 'flex',
    flexWrap: 'wrap',
    paddingRight: 0,
    paddingLeft: 0,
    paddingBottom: 0,
  },
});

class LoginRegisterContainer extends Component {
  state = {
    expand: 'loginPanel',
  };

  expander = panel => (event, expand) => {
    this.setState({
      expand: expand ? panel : false,
    });
  };

  render() {
    const { classes } = this.props;
    const { expand } = this.state;

    return (
      <Card className={classes.itemWidth}>
        <CardContent>
          <Typography className={classes.heading} variant="h6">
            Welcome in Grey Knight
          </Typography>
        </CardContent>
        <Divider />
        <CardActions>
          <Grid container spacing={8} direction="column">
            <Grid item xs={12}>
              <ExpansionPanel expanded={expand === 'loginPanel'} onChange={this.expander('loginPanel')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography className={classes.secondaryHeading} variant="h1">LogIn</Typography>
                </ExpansionPanelSummary>
                <Divider />
                <ExpansionPanelDetails className={classes.textContainer}>

                  <LoginItem />

                </ExpansionPanelDetails>
              </ExpansionPanel>
            </Grid>
            <Grid item xs={12}>
              <ExpansionPanel expanded={expand === 'registerPanel'} onChange={this.expander('registerPanel')}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                  <Typography className={classes.secondaryHeading} variant="h1">Register</Typography>
                </ExpansionPanelSummary>
                <Divider />
                <ExpansionPanelDetails className={classes.textContainer}>

                  <RegisterItem />

                </ExpansionPanelDetails>
              </ExpansionPanel>
            </Grid>
          </Grid>
        </CardActions>
      </Card>
    );
  }
}

LoginRegisterContainer.propTypes = {
  ...withStyles.propTypes,
};

export default withStyles(styles)(LoginRegisterContainer);