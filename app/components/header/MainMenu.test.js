import React from 'react';
import { shallow } from 'enzyme';

import MainMenu from './MainMenu';

describe('<MainMenu />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<MainMenu
      mainMenuToggle={true}
      closeMainMenu={() => undefined}/>);

    expect(wrapper).toMatchSnapshot();
  });
});