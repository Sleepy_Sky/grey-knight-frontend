import * as constants from '../../constatnts';

const initialStore = {
  mainMenuToggle: false,
  loginRegisterToggle: null,
};

const headerReducer = (store = initialStore, action = {}) => {
  switch (action.type) {
    case constants.OPEN_MAIN_MENU:
      return {
        ...store,
        mainMenuToggle: action.mainMenuToggle,
      };
    case constants.CLOSE_MAIN_MENU:
      return {
        ...store,
        mainMenuToggle: action.mainMenuToggle,
      };
    case constants.OPEN_LOGIN_REGISTER:
      return {
        ...store,
        loginRegisterToggle: action.loginRegisterToggle,
      };
    case constants.CLOSE_LOGIN_REGISTER:
      return {
        ...store,
        loginRegisterToggle: action.loginRegisterToggle,
      };
    default:
      return store;
  }
};

export default headerReducer;