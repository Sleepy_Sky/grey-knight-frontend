import React, {Component} from 'react';
import { string, func } from 'prop-types';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import Header_v1 from './header/container';
import ContentContainer from './content/ContentContainer';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#18121e',
    },
    secondary: {
      main: '#233237',
      light: 'rgba(35, 50, 55, 0.05)',
    },
    error: {
      main: '#b00020',
    },
  },
  typography: {
    useNextVariants: true,
  },
});
//#18121e - navy blue
//#233237 - gunmetal
//#984b43 - rusty red
//#eac67a - worm yellow

export const SelectionContext = React.createContext();

class Layout extends Component {
  render() {
    const { selectContent, activeContent } = this.props;
    return (
      <MuiThemeProvider theme={theme}>
        <SelectionContext.Provider value={selectContent} >
          <Header_v1/>
        </SelectionContext.Provider>
        <ContentContainer
          activeContent={activeContent}/>
      </MuiThemeProvider>
    );
  }
}

Layout.propTypes = {
  selectContent: func.isRequired,
  activeContent: string.isRequired,
};

export default Layout;