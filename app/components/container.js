import { connect } from 'react-redux';
import { selectContent } from './actions';
import Layout from './Layout';

const mapStateToProps = state => ({
  activeContent: state.globalReducer.activeContent,
});

const mapDispatchToProps = dispatch => ({
  selectContent: event => dispatch(selectContent(event)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Layout);
