import { SELECTED_CONTENT } from '../constatnts';

export const initialStore = {
  activeContent: 'characterCreator',
};

const globalReducer = (store = initialStore, action = {}) => {
  switch (action.type) {
    case SELECTED_CONTENT:
      return {
        ...store,
        activeContent: action.activeContent,
      };
    default:
      return store;
  }
};

export default globalReducer;