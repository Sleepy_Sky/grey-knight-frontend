import React from 'react';
import { string, bool, oneOfType } from 'prop-types';
import { withStyles, AppBar, Typography, CircularProgress } from'@material-ui/core';

const styles = theme => ({
  root: {
    margin: '5px 0 0 0',
    height: 34,
  },
  text: {
    width: '100%',
  },
  loader: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  spinner: {
    color: theme.palette.common.white,
  },
});

const InfoPanel = (props) => {
  const { classes, data } = props;
  return (
    <AppBar className={classes.root} position="static">
      {
        (data) ?
          (
            <Typography align="center" variant="h6" color="inherit">
              {data}
            </Typography>
          )
          :
          (
            <div className={classes.loader}>
              <CircularProgress className={classes.spinner} size={30} value={0}/>
            </div>
          )
      }
    </AppBar>
  );
};

InfoPanel.propTypes = {
  ...withStyles.propTypes,
  data: oneOfType([string, bool]).isRequired,
};

export default withStyles(styles)(InfoPanel);



