import React from 'react';
import { shallow } from 'enzyme';

import RPGIcon from './RPGIcon';

describe('<RPGIcon />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<RPGIcon iconRPG=""/>);

    expect(wrapper).toMatchSnapshot();
  });
});