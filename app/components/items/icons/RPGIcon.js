import React from 'react';
import '../../../../node_modules/rpg-awesome/css/rpg-awesome.css';
import { string } from 'prop-types';

const RPGIcon = ({ iconRPG }) => (
  <i className={iconRPG} />
);

RPGIcon.propTypes = {
  iconRPG: string.isRequired,
};

export default RPGIcon;