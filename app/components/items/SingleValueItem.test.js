import React from 'react';
import { shallow } from 'enzyme';

import SingleValueItem from './SingleValueItem';

describe('<SingleValueItem />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<SingleValueItem title="" data=""/>);

    expect(wrapper).toMatchSnapshot();
  });
});