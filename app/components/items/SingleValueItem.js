import React from 'react';
import { string } from 'prop-types';
import { Paper, Typography, Divider } from '@material-ui/core';

const SingleValueItem = ({ title, data }) => (
  <Paper>
    <Typography variant="subtitle1" align="center">{title}</Typography>
    <Divider />
    <Typography variant="body2" align="center">{data}</Typography>
  </Paper>
);

SingleValueItem.propTypes = {
  title: string.isRequired,
  data: string.isRequired,
};

export default SingleValueItem;