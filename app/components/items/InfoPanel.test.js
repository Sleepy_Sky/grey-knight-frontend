import React from 'react';
import { shallow } from 'enzyme';

import InfoPanel from './InfoPanel';

describe('<InfoPanel />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<InfoPanel data=""/>);

    expect(wrapper).toMatchSnapshot();
  });
});