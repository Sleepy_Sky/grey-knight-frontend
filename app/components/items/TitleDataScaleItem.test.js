import React from 'react';
import { shallow } from 'enzyme';

import TitleDataScaleItem from './TitleDataScaleItem';

describe('<TitleDataScaleItem />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<TitleDataScaleItem scale={6} title="" data=""/>);

    expect(wrapper).toMatchSnapshot();
  });
});