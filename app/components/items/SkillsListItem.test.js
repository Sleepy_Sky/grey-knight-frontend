import React from 'react';
import { shallow } from 'enzyme';

import SkillsListItem from './SkillsListItem';

describe('<SkillsListItem />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<SkillsListItem
      scaleXS={6}
      scaleSM={4}
      titles={ { title: '', contents: [''], } } data={ { level: 0, contents: [0], } }/>);

    expect(wrapper).toMatchSnapshot();
  });
});