import React from 'react';
import { string, number } from 'prop-types';
import { withStyles, Paper, Typography, Divider, Grid } from '@material-ui/core';

const styles = () => ({
  heading: {
    fontWeight: 'bold',
  }
});

const TitleDataScaleItem = ({ classes, scale, title, data }) => {
  return (
    <Grid item xs={scale}>
      <Paper>
        <Typography
          className={classes.heading}
          variant="subtitle1"
          align="center">{title}</Typography>
        <Divider />
        <Typography
          variant="body2"
          align="center">{data}</Typography>
      </Paper>
    </Grid>
  );
};

TitleDataScaleItem.propTypes = {
  ...withStyles.propTypes,
  scale: number.isRequired,
  title: string.isRequired,
  data: string.isRequired,
};

export default withStyles(styles)(TitleDataScaleItem);