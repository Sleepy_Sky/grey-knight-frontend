import React from 'react';
import { number, string, shape, arrayOf } from 'prop-types';
import { withStyles, Divider, Grid, Typography } from '@material-ui/core';

const styles = theme => ({
  headingBG: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    backgroundColor: theme.palette.secondary.light,
    padding: '0 0 0 10px',
    fontWeight: 'bold',
  },
  statsContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: '0 0 0 10px',
  },
  headStatTitle: {
    width: '90%',
    fontWeight: 'bold',
  },
  headStatValue: {
    width: '10%',
    fontWeight: 'bold',
  },
  emptyOne: {
    width: '10%',
  },
  statTitle: {
    width: '80%',
  },
  statValue: {
    width: '10%',
  },
});

const SkillsListItem = ({ classes, scaleXS, scaleSM, titles, data }) => {
  return (
    <Grid item xs={scaleXS} sm={scaleSM}>
      <div className={classes.headingBG}>
        <Typography
          className={classes.headStatTitle}
          variant="subtitle1">{titles.title}</Typography>
        <Typography
          className={classes.headStatValue}
          variant="subtitle1"
          align="center">{data.level}</Typography>
      </div>
      <Divider/>
      {titles.contents.map((element, index) => (
        <div className={classes.statsContainer} key={index}>
          <div className={classes.emptyOne} />
          <Typography
            className={classes.statTitle}
            variant="body2">{element}</Typography>
          <Typography
            className={classes.statValue}
            variant="body2"
            align="center">{data.contents[index]}</Typography>
        </div>
      ))}
    </Grid>
  );
};

SkillsListItem.propTypes = {
  ...withStyles.propTypes,
  scaleXS: number.isRequired,
  scaleSM: number.isRequired,
  titles: shape({
    title: string.isRequired,
    contents: arrayOf(string).isRequired,
  }).isRequired,
  data: shape({
    level: number.isRequired,
    contents: arrayOf(number).isRequired,
  }).isRequired,
};

export default withStyles(styles)(SkillsListItem);