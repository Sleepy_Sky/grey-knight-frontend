import React from 'react';
import { shallow } from 'enzyme';

import Layout from '../Layout';

describe('<Layout />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<Layout
      selectContent={() => undefined}
      activeContent=""/>);

    expect(wrapper).toMatchSnapshot();
  });
});