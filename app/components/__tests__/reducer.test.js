import reducer, { initialStore } from '../reducer';
import { SELECTED_CONTENT } from '../../constatnts';

describe('Layout reducer', () => {
  it('should return the initial store', () => {
    expect(reducer(undefined, {})).toEqual(initialStore);
  });

  it('should handle SELECTED_CONTENT action', () => {
    const action = {
      type: SELECTED_CONTENT,
      activeContent: 'someContent',
    };

    expect(reducer(undefined, action))
      .toEqual({
        ...initialStore,
        activeContent: 'someContent',
      });
  });
});