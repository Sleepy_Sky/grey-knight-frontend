import { SELECTED_CONTENT } from '../constatnts';

export const selectContent = event => {
  const toReturn = event.currentTarget.id;
  return ({
    type: SELECTED_CONTENT,
    activeContent: toReturn,
  });
};