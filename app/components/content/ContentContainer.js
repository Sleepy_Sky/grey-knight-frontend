import React, { Component, Fragment } from 'react';
import { string } from 'prop-types';
import CharacterSheetContainer from './characterSheet/container';
import CharacterCreatorContainer from './characterCreator/container';

class ContentContainer extends Component {
  render() {
    const { activeContent } = this.props;
    return(
      <Fragment>
        {(activeContent === 'characterSheet') && <CharacterSheetContainer /> }
        {(activeContent === 'characterCreator') && <CharacterCreatorContainer/> }
      </Fragment>
    );
  }
}

ContentContainer.propTypes = {
  activeContent: string.isRequired,
};

export default ContentContainer;