import React from 'react';
import { shallow } from 'enzyme';

import ContentContainer from '../ContentContainer';

describe('<ContentContainer />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<ContentContainer activeContent=""/>);

    expect(wrapper).toMatchSnapshot();
  });
});