import React from 'react';
import { shallow } from 'enzyme';

import MenuPC from './MenuPC';

describe('<MenuPC />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<MenuPC
      titles={false}
      handler={() => undefined}
      currentClickedTab=""/>);

    expect(wrapper).toMatchSnapshot();
  });
});