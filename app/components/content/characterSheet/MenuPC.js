import React from 'react';
import { func, string, bool, shape, oneOfType } from 'prop-types';
import { withStyles, Paper, MenuList, MenuItem, ListItemIcon, Typography, CircularProgress } from '@material-ui/core';
import RPGIcon from '../../items/icons/RPGIcon';

const styles = theme => ({
  root: {
    [theme.breakpoints.down(600)]: {
      display: 'none',
    },
  },
  loaderContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
});

const MenuPC = props => {
  const { classes, handler, currentClickedTab, titles } = props;

  return (
    <Paper className={classes.root} square>
      <MenuList>
        {
          (titles) ? (
            <MenuItem
              id="charInfo"
              onClick={handler}
              selected={('charInfo' === currentClickedTab)}>
              <ListItemIcon>
                <RPGIcon iconRPG="ra ra-player" />
              </ListItemIcon>
              <Typography
                variant='inherit'
                noWrap>{titles.kid}</Typography>
            </MenuItem>
          ) : (
            <div className={classes.loaderContainer}>
              <CircularProgress
                size={30}
                color="secondary"/>
            </div>
          )
        }
        {
          (titles) ? (
            <MenuItem
              id="statistic"
              onClick={handler}
              selected={('statistic' === currentClickedTab)}>
              <ListItemIcon>
                <RPGIcon iconRPG="ra ra-book" />
              </ListItemIcon>
              <Typography
                variant='inherit'
                noWrap>{titles.skills}</Typography>
            </MenuItem>
          ) : (
            <div className={classes.loaderContainer}>
              <CircularProgress
                size={30}
                color="secondary"/>
            </div>
          )
        }
        {
          (titles) ? (
            <MenuItem
              id="relations"
              onClick={handler}
              selected={('relations' === currentClickedTab)}>
              <ListItemIcon>
                <RPGIcon iconRPG="ra ra-double-team"/>
              </ListItemIcon>
              <Typography
                variant='inherit'
                noWrap>{titles.relations}</Typography>
            </MenuItem>
          ) : (
            <div className={classes.loaderContainer}>
              <CircularProgress
                size={30}
                color="secondary"/>
            </div>
          )
        }
        {
          (titles) ? (
            <MenuItem
              id="equipment"
              onClick={handler}
              selected={('equipment' === currentClickedTab)}>
              <ListItemIcon>
                <RPGIcon iconRPG="ra ra-hammer" />
              </ListItemIcon>
              <Typography
                variant='inherit'
                noWrap>{titles.items}</Typography>
            </MenuItem>
          ) : (
            <div className={classes.loaderContainer}>
              <CircularProgress
                size={30}
                color="secondary"/>
            </div>
          )
        }
      </MenuList>
    </Paper>
  );
};

MenuPC.propTypes = {
  ...withStyles.propTypes,
  titles: oneOfType([
    bool,
    shape({
      kid: string,
      skills: string,
      relations: string,
      items: string,
    })
  ]).isRequired,
  handler: func.isRequired,
  currentClickedTab: string.isRequired,
};

export default withStyles(styles)(MenuPC);