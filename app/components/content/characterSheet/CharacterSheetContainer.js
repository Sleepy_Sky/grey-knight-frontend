import React, { Component } from 'react';
import { object, func, string } from 'prop-types';
import { Grid } from '@material-ui/core';
import InfoPanel from '../../items/InfoPanel';
import MenuMobile from './MenuMobile';
import MenuPC from './MenuPC';
import CharInfo from './tabs/CharInfo';
import Skills from './tabs/Skills';

class CharacterSheetContainer extends Component {
  state = {
    value: 0,
  };

  componentDidMount() {
    this.props.loadCharacterSheet();
  }


  mobileMenuHandleChange = (event, value) => {
    this.setState({ value });
  };

  render() {
    const { titlesData, personalData, selectMenuTab, selectedMenuTab } = this.props;
    const { value } = this.state;

    //  Data that goes to the InfoPanel,
    //  probably name of a character and maybe name of a session.
    const infoPanelData = (titlesData) ? titlesData.title : false;

    //  Data that goes to the MenuPC component,
    //  only names of tabs.
    const menuPCData = (titlesData) ? titlesData.menu_ENG_titles : false;

    //  Data that goes to the CharInfo component.
    const charInfoTitles = (titlesData) ? titlesData.charInfo_ENG_titles : false;
    const charInfoPersonalData = (personalData) ? personalData.charInfo_data : false;

    //  Data that goes to the Skills component.
    const skillsTitles = (titlesData) ? titlesData.skills_ENG_titles : false;
    const skillsPersonalData = (personalData) ? personalData.skills_data : false;


    // ToDo: end tabs components integration with redux props.
    return(
      <Grid container justify="center">
        <Grid item xs={12} md={10} lg={8} xl={6}>
          <InfoPanel data={infoPanelData}/>
          <MenuMobile
            value={value}
            handleChange={this.mobileMenuHandleChange}
            handler={selectMenuTab}/>
          <Grid
            container
            justify="flex-start"
            alignItems="flex-start"
            spacing={8}>

            <Grid item sm={2}>
              <MenuPC
                handler={selectMenuTab}
                currentClickedTab={selectedMenuTab}
                titles={menuPCData}/>
            </Grid>
            <Grid item xs={12} sm={10}>
              {
                ((selectedMenuTab === 'charInfo') && (charInfoTitles && charInfoPersonalData)) &&
                <CharInfo
                  titles={charInfoTitles}
                  data={charInfoPersonalData}/>
              }
              {
                ((selectedMenuTab === 'statistic') && (skillsTitles && skillsPersonalData)) &&
                <Skills
                  titles={skillsTitles}
                  data={skillsPersonalData}/>
              }
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    );
  }
}

CharacterSheetContainer.propTypes = {
  loadCharacterSheet: func.isRequired,
  selectMenuTab: func.isRequired,
  selectedMenuTab: string.isRequired,
  titlesData: object, //ToDo: Change to shape when the object will be finished
  personalData: object, //ToDo Change to shape when the object will be finished
};

export default CharacterSheetContainer;