export const titlesData = {
  title: 'required info like character name',
  menu_ENG_titles: {
    kid: 'Kid',
    skills: 'Skills',
    relations: 'Relations',
    items: 'Items',
  },
  charInfo_ENG_titles: {
    name: 'Name',
    nickname: 'Nickname',
    type: 'Type',
    age: 'Age',
    birthday: 'Birthday',
    luckPoints: 'Luck Points',
    drive: 'Drive',
    anchor: 'Anchor',
    problem: 'Problem',
    pride: 'Pride',
    favoriteSong: 'Favorite Song',
    description: 'Description',
    hideout: 'Hideout',
  },
  skills_ENG_titles: {
    body: {
      title: 'Body',
      contents: ['Sneak', 'Force', 'Move'],
    },
    tech: {
      title: 'Tech',
      contents: ['Tinker', 'Program', 'Calculate'],
    },
    heart: {
      title: 'Heart',
      contents: ['Contact', 'Charm', 'Lead'],
    },
    mind: {
      title: 'Mind',
      contents: ['Investigate', 'Comprehend', 'Empathize'],
    },
    conditions: {
      title: 'Conditions',
      upset: 'Upset',
      scared: 'Scared',
      exhausted: 'Exhausted',
      injured: 'Injured',
      broken: 'Broken',
    }
  },
  relations_ENG_titles: {
    titleKids: 'Kids',
    titleNPCs: 'NPC\'s',
  },
  characterCreator_titles: {
    header: 'Character Creator',
    steps: ['Basic Information', 'Skills'],
    basicInformation: {
      name: 'Name',
      nickname: 'Nickname',
      type: 'Type',
      typeOptions: ['Bookworm', 'Computer Geek', 'Hick', 'Jock', 'Popular Kid', 'Rocker', 'Troublemaker', 'Weirdo'],
      age: 'Age',
      ageOptions: [10, 11, 12, 13, 14, 15],
      luckPoints: 'Luck Points',
    },
  },
};

export const personalData = {
  charInfo_data: {
    name: 'Joshua',
    type: 'Bookworm',
    age: '12',
    luckPoints: '3/3',
    drive: 'I need something to brag about.',
    anchor: 'Teacher, Mrs. Robinson',
    problem: 'Nobody tells me how my dad died.',
    pride: 'Im the smartest kid in school.',
    favoriteSong: 'Favorite Song',
    description: 'Just a normal guy who likes books and being alone.',
    hideout: 'The Tree House',
  },
  skills_data: {
    body: {
      level: 5,
      contents: [2, 1, 2],
    },
    tech: {
      level: 2,
      contents: [0, 0, 2],
    },
    heart: {
      level: 2,
      contents: [2, 1, 0],
    },
    mind: {
      level: 4,
      contents: [2, 3, 3],
    },
    conditions: {
      upset: false,
      scared: false,
      exhausted: true,
      injured: false,
      broken: false,
    }
  },
  relations_data: {
    kids: [
      {name: 'Allan', role: 'Friend'},
      {name: 'Dicon', role: 'Bully'},
      {name: 'Jessica', role: 'Classmate'}
    ],
    NPCs: [
      {name: 'Mrs. Owena', role: ''}
    ],
  },
};
