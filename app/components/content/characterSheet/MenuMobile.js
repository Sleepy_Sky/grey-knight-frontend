import React from 'react';
import { number, func } from 'prop-types';
import { withStyles, Paper, Tabs, Tab } from '@material-ui/core';
import RPGIcon from '../../items/icons/RPGIcon';

const styles = (theme) => ({
  iconStyle: {
    fontSize: 30,
  },
  mobileEnd: {
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
});

const MenuMobile = (props) => {
  const { classes, value, handleChange, handler } = props;

  return (
    <Paper className={classes.mobileEnd} square>
      <Tabs
        value={value}
        centered={true}
        variant="fullWidth"
        onChange={handleChange}>
        <Tab
          id="charInfo"
          className={classes.iconStyle}
          icon={ <RPGIcon iconRPG="ra ra-player" /> }
          onClick={handler}/>
        <Tab
          id="statistic"
          className={classes.iconStyle}
          icon={ <RPGIcon iconRPG="ra ra-book" /> }
          onClick={handler}/>
        <Tab
          id="relations"
          className={classes.iconStyle}
          icon={ <RPGIcon iconRPG="ra ra-double-team"/> }
          onClick={handler}/>
        <Tab
          id="equipment"
          className={classes.iconStyle}
          icon={ <RPGIcon iconRPG="ra ra-hammer" /> }
          onClick={handler}/>
      </Tabs>
    </Paper>
  );
};

MenuMobile.propTypes = {
  ...withStyles.propTypes,
  value: number.isRequired,
  handleChange: func.isRequired,
  handler: func.isRequired,
};

export default withStyles(styles)(MenuMobile);