import * as constants from '../../../constatnts';

export const characterSheetRequest = () => ({
  type: constants.CHARACTER_SHEET_REQUEST,
});

export const characterSheetRequestSuccess = () => ({
  type: constants.CHARACTER_SHEET_REQUEST_SUCCESS,
});

export const characterSheetRequestError = () => ({
  type: constants.CHARACTER_SHEET_REQUEST_ERROR,
});

export const selectMenuTab = (event) => {
  const toReturn = event.currentTarget.id;
  return ({
    type: constants.SELECTED_MENU_TAB,
    selectedMenuTab: toReturn,
  });
};

export const loadCharacterSheetData = (dispatch) => () => {
  dispatch(characterSheetRequest());
  //  fetch, if okay: success, if not; error
  dispatch(characterSheetRequestSuccess());
};

