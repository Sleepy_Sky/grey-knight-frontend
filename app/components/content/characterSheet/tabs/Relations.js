import React from 'react';
import { object, array } from 'prop-types';
import { withStyles, Paper, Grid, Typography, Divider } from '@material-ui/core';

const styles = theme => ({
  mainContainer: {
    padding: theme.spacing.unit,
  },
  heading: {
    display: 'flex',
    justifyContent: 'center',
    width: '100%',
    height: '30px',
    backgroundColor: theme.palette.secondary.light,
  },
  headText: {
    fontWeight: 'bold',
  },
  content: {
    display: 'flex',
    justifyContent: 'flex-start',
    padding: '0 0 0 10px',
  },
});


const Relations = ({ classes, titles, data }) => (
  <Paper className={classes.mainContainer}>
    <Grid container spacing={8}>
      <Grid item xs={12}>
        <Paper>
          <div className={classes.heading}>
            <Typography className={classes.headText}>{titles.titleKids}</Typography>
          </div>
          <Divider/>
          {
            data.kids.map((element, index) => (
              <div className={classes.content} key={index}>
                <Typography variant="body2">{element}</Typography>
              </div>
            ))
          }
        </Paper>
      </Grid>
      <Grid item xs={12}>

      </Grid>
    </Grid>
  </Paper>
);

Relations.propTypes = {
  ...withStyles.propTypes,
  titles: object.isRequired,
  data: array.isRequired,
};

export default withStyles(styles)(Relations);