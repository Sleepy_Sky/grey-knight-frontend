import React from 'react';
import { shallow } from 'enzyme';

import Skills from './Skills';

describe('<Skills />', () => {
  it('should match snapshot', () => {
    const testTitles = {
      body: {
        title: '',
        contents: [''],
      },
      tech: {
        title: '',
        contents: [''],
      },
      heart: {
        title: '',
        contents: [''],
      },
      mind: {
        title: '',
        contents: [''],
      },
      conditions: {
        title: '',
        upset: '',
        scared: '',
        exhausted: '',
        injured: '',
        broken: '',
      }
    };

    const testData = {
      body: {
        level: 0,
        contents: [0],
      },
      tech: {
        level: 0,
        contents: [0],
      },
      heart: {
        level: 0,
        contents: [0],
      },
      mind: {
        level: 0,
        contents: [0],
      },
      conditions: {
        upset: true,
        scared: true,
        exhausted: true,
        injured: true,
        broken: true,
      },
    };
    const wrapper = shallow(<Skills titles={testTitles} data={testData}/>);

    expect(wrapper).toMatchSnapshot();
  });
});
