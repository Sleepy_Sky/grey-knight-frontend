import React from 'react';
import { shape, string, number, bool, arrayOf } from 'prop-types';
import { withStyles, Paper, Grid } from '@material-ui/core';
import SkillsListItem from '../../../items/SkillsListItem';
import ConditionsTable from './items/ConditionsTable';

const styles = theme => ({
  mainContainer: {
    padding: theme.spacing.unit,
  },
});

const Skills = props => {
  const { classes, titles, data } = props;
  return (
    <Paper className={classes.mainContainer} square>
      <Grid container spacing={8}>
        <ConditionsTable
          titles={titles.conditions}
          data={data.conditions}/>
        <SkillsListItem
          scaleXS={12}
          scaleSM={6}
          titles={titles.body}
          data={data.body}/>
        <SkillsListItem
          scaleXS={12}
          scaleSM={6}
          titles={titles.tech}
          data={data.tech}/>
        <SkillsListItem
          scaleXS={12}
          scaleSM={6}
          titles={titles.heart}
          data={data.heart}/>
        <SkillsListItem
          scaleXS={12}
          scaleSM={6}
          titles={titles.mind}
          data={data.mind}/>
      </Grid>
    </Paper>
  );
};

Skills.propTypes = {
  titles: shape({
    body: shape({
      title: string.isRequired,
      contents: arrayOf(string).isRequired,
    }),
    tech: shape({
      title: string.isRequired,
      contents: arrayOf(string).isRequired,
    }),
    heart: shape({
      title: string.isRequired,
      contents: arrayOf(string).isRequired,
    }),
    mind: shape({
      title: string.isRequired,
      contents: arrayOf(string).isRequired,
    }),
    conditions: shape({
      title: string.isRequired,
      upset: string.isRequired,
      scared: string.isRequired,
      exhausted: string.isRequired,
      injured: string.isRequired,
      broken: string.isRequired,
    }),
  }).isRequired,
  data: shape({
    body: shape({
      level: number.isRequired,
      contents: arrayOf(number).isRequired,
    }),
    tech: shape({
      level: number.isRequired,
      contents: arrayOf(number).isRequired,
    }),
    heart: shape({
      level: number.isRequired,
      contents: arrayOf(number).isRequired,
    }),
    mind: shape({
      level: number.isRequired,
      contents: arrayOf(number).isRequired,
    }),
    conditions: shape({
      upset: bool.isRequired,
      scared: bool.isRequired,
      exhausted: bool.isRequired,
      injured: bool.isRequired,
      broken: bool.isRequired,
    }),
  }).isRequired,
  ...withStyles.propTypes,
};

export default withStyles(styles)(Skills);