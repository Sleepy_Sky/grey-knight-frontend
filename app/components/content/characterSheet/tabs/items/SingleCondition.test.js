import React from 'react';
import { shallow } from 'enzyme';

import SingleCondition from './SingleCondition';

describe('<SingleCondition />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<SingleCondition title="" bool={true}/>);

    expect(wrapper).toMatchSnapshot();
  });
});