import React from 'react';
import { string, bool } from 'prop-types';
import { withStyles, Grid, Typography } from '@material-ui/core';
import { CheckBox, CheckBoxOutlineBlank } from '@material-ui/icons';

const styles = theme => ({
  statContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    padding: '0 0 0 10px',
    borderWidth: '0 2px 0 2px',
    borderStyle: 'solid',
    color: theme.palette.secondary.light,
  },
  statTitle: {
    width: '80%',
  },
  statValue: {
    width: '20%',
    display: 'flex',
    justifyContent: 'center'
  },
});

const SingleCondition = ({ classes, title, bool }) => (
  <Grid item xs={6} sm={3}>
    <div className={classes.statContainer}>
      <Typography
        className={classes.statTitle}
        variant="body2">{title}</Typography>
      <div className={classes.statValue}>
        {bool && <CheckBox color="error"/>}
        {!bool && <CheckBoxOutlineBlank/>}
      </div>
    </div>
  </Grid>
);

SingleCondition.propTypes = {
  ...withStyles.propTypes,
  title: string.isRequired,
  bool: bool.isRequired,
};

export default withStyles(styles)(SingleCondition);