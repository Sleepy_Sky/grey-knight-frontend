import React from 'react';
import { shallow } from 'enzyme';

import ConditionsTable from './ConditionsTable';

describe('<ConditionsTable />', () => {
  it('should match snapshot', () => {
    const testTitles = {
      title: '',
      upset: '',
      scared: '',
      exhausted: '',
      injured: '',
      broken: '',
    };

    const testData = {
      upset: true,
      scared: true,
      exhausted: true,
      injured: true,
      broken: true,
    };

    const wrapper = shallow(<ConditionsTable titles={testTitles} data={testData}/>);

    expect(wrapper).toMatchSnapshot();
  });
});