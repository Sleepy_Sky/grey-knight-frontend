import React from 'react';
import { shape, string, bool } from 'prop-types';
import { withStyles, Grid, Typography, Divider } from '@material-ui/core';
import SingleCondition from './SingleCondition';

const styles = theme => ({
  heading: {
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: theme.palette.secondary.light,
  },
  headText: {
    fontWeight: 'bold',
  },
  brokenContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    backgroundColor: theme.palette.error.main,
    borderRadius: '10px',
  },
  brokenText: {
    fontWeight: 'bold',
  },
});

const ConditionsTable = ({ classes, titles, data }) => {
  return (
    <Grid item xs={12}>
      {
        !data.broken &&
        <div className={classes.heading}>
          <Typography
            className={classes.headText}
            variant="subtitle1"
            align="center">{titles.title}</Typography>
        </div>
      }
      <Divider/>
      <Grid container>
        {
          data.broken &&
          <Grid item xs={12}>
            <div className={classes.brokenContainer}>
              <Typography
                className={classes.brokenText}
                variant="subtitle1">{titles.broken}</Typography>
            </div>
          </Grid>
        }
        {!data.broken && <SingleCondition title={titles.upset} bool={data.upset}/>}
        {!data.broken && <SingleCondition title={titles.scared} bool={data.scared}/>}
        {!data.broken && <SingleCondition title={titles.exhausted} bool={data.exhausted}/>}
        {!data.broken && <SingleCondition title={titles.injured} bool={data.injured}/>}


      </Grid>
    </Grid>
  );
};

ConditionsTable.propTypes = {
  ...withStyles.propTypes,
  titles: shape({
    title: string.isRequired,
    upset: string.isRequired,
    scared: string.isRequired,
    exhausted: string.isRequired,
    injured: string.isRequired,
    broken: string.isRequired,
  }).isRequired,
  data: shape({
    upset: bool.isRequired,
    scared: bool.isRequired,
    exhausted: bool.isRequired,
    injured: bool.isRequired,
    broken: bool.isRequired,
  }).isRequired,
};

export default withStyles(styles)(ConditionsTable);