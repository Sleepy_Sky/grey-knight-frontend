import React from 'react';
import { shallow } from 'enzyme';

import CharInfo from './CharInfo';

describe('<CharInfo />', () => {
  it('should match snapshot', () => {
    const test = {
      name: '',
      type: '',
      age: '',
      luckPoints: '',
      drive: '',
      anchor: '',
      problem: '',
      pride: '',
      favoriteSong: '',
      description: '',
      hideout: '',
    };

    const wrapper = shallow(
      <CharInfo titles={test} data={test}/>);

    expect(wrapper).toMatchSnapshot();
  });
});