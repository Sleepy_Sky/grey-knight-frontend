import React from 'react';
import { shallow } from 'enzyme';

import Relations from './Relations';

describe('<Relations />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<Relations titles={ {} } data={ [] } />);

    expect(wrapper).toMatchSnapshot();
  });
});