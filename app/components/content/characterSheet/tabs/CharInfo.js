import React from 'react';
import { shape, string } from 'prop-types';
import { withStyles, Paper, Grid } from '@material-ui/core';
import TitleDataScaleItem from '../../../items/TitleDataScaleItem';

const styles = theme => ({
  mainContainer: {
    padding: theme.spacing.unit,
  },
});

const CharInfo = (props) => {
  const { classes, titles, data } = props;
  return (
    <Paper className={classes.mainContainer} square>
      <Grid container spacing={8}>
        <TitleDataScaleItem
          data={data.name}
          title={titles.name}
          scale={12}/>
        <TitleDataScaleItem
          data={data.type}
          title={titles.type}
          scale={12}/>
        <TitleDataScaleItem
          data={data.age}
          title={titles.age}
          scale={6}/>
        <TitleDataScaleItem
          data={data.luckPoints}
          title={titles.luckPoints}
          scale={6}/>
        <TitleDataScaleItem
          data={data.hideout}
          title={titles.hideout}
          scale={12}/>
        <TitleDataScaleItem
          data={data.anchor}
          title={titles.anchor}
          scale={12}/>
        <TitleDataScaleItem
          data={data.drive}
          title={titles.drive}
          scale={12}/>
        <TitleDataScaleItem
          data={data.problem}
          title={titles.problem}
          scale={12}/>
        <TitleDataScaleItem
          data={data.pride}
          title={titles.pride}
          scale={12}/>
        <TitleDataScaleItem
          data={data.favoriteSong}
          title={titles.favoriteSong}
          scale={12}/>
        <TitleDataScaleItem
          data={data.description}
          title={titles.description}
          scale={12}/>
      </Grid>
    </Paper>
  );
};

CharInfo.propTypes = {
  titles: shape({
    name: string.isRequired,
    type: string.isRequired,
    age: string.isRequired,
    luckPoints: string.isRequired,
    drive: string.isRequired,
    anchor: string.isRequired,
    problem: string.isRequired,
    pride: string.isRequired,
    favoriteSong: string.isRequired,
    description: string.isRequired,
    hideout: string.isRequired,
  }).isRequired,
  data: shape({
    name: string.isRequired,
    type: string.isRequired,
    age: string.isRequired,
    luckPoints: string.isRequired,
    drive: string.isRequired,
    anchor: string.isRequired,
    problem: string.isRequired,
    pride: string.isRequired,
    favoriteSong: string.isRequired,
    description: string.isRequired,
    hideout: string.isRequired,
  }).isRequired,
  ...withStyles.propTypes,
};

export default withStyles(styles)(CharInfo);