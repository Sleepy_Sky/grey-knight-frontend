import React from 'react';
import { shallow } from 'enzyme';

import CharacterSheetContainer from './container';

describe('<CharacterSheetContainer />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<CharacterSheetContainer
      loadCharacterSheet={() => undefined}
      selectMenuTab={() => undefined}
      selectedMenuTab=""
      titlesData={ {} }
      personalData={ {} }/>);

    expect(wrapper).toMatchSnapshot();
  });
});