import * as constants from '../../../constatnts';
import * as mockData from './mockData';

const initialStore = {
  titlesData: null,
  personalData: null,
  selectedMenuTab: 'charInfo',
};

const characterSheetReducer = (store = initialStore, action = {}) => {
  switch (action.type) {
    case constants.CHARACTER_SHEET_REQUEST_SUCCESS:
      return {
        ...store,
        titlesData: mockData.titlesData,
        personalData: mockData.personalData,
      };
    case constants.SELECTED_MENU_TAB:
      return {
        ...store,
        selectedMenuTab: action.selectedMenuTab,
      };
    default:
      return store;
  }
};

export default characterSheetReducer;