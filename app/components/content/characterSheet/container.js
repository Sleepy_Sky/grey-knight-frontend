import { connect } from 'react-redux';
import { loadCharacterSheetData, selectMenuTab } from './actions';
import CharacterSheetContainer from './CharacterSheetContainer';

const mapStateToProps = (state) => ({
  titlesData: state.characterSheetReducer.titlesData,
  personalData: state.characterSheetReducer.personalData,
  selectedMenuTab: state.characterSheetReducer.selectedMenuTab,
});

const mapDispatchToProps = (dispatch) => ({
  loadCharacterSheet: loadCharacterSheetData(dispatch),
  selectMenuTab: (event) => dispatch(selectMenuTab(event)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CharacterSheetContainer);