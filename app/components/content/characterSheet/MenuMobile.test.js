import React from 'react';
import { shallow } from 'enzyme';

import MenuMobile from './MenuMobile';

describe('<MenuMobile />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<MenuMobile
      value={0}
      handleChange={() => undefined}
      handler={() => undefined}/>);

    expect(wrapper).toMatchSnapshot();
  });
});