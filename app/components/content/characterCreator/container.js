import { connect } from 'react-redux';
import { loadCharacterCreatorData } from './actions';
import CharacterCreatorContainer from './CharacterCreatorContainer';

const mapStateToProps = state => ({
  characterCreatorTitles: state.characterCreatorReducer.characterCreatorTitles,
});

const mapDispatchToProps = dispatch => ({
  loadCharacterCreator: loadCharacterCreatorData(dispatch),
});

export default connect(mapStateToProps, mapDispatchToProps)(CharacterCreatorContainer);