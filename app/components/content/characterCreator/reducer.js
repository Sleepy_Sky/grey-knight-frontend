import * as constants from '../../../constatnts';

const initialStore = {
  characterCreatorTitles: {
    header: 'Character Creator',
    steps: ['Basic Information', 'Skills'],
    basicInformation: {
      name: 'Name',
      nickname: 'Nickname',
      type: 'Type',
      typeOptions: ['Bookworm', 'Computer Geek', 'Hick', 'Jock', 'Popular Kid', 'Rocker', 'Troublemaker', 'Weirdo'],
      age: 'Age',
      ageOptions: [10, 11, 12, 13, 14, 15],
      luckPoints: 'Luck Points',
    },
  }
};

const characterCreatorReducer = (store = initialStore, action = {}) => {
  switch (action.type) {
    case constants.CHARACTER_CREATOR_REQUEST_SUCCESS:
      return {
        ...store,
        loadCharacterCreatorData: action.loadCharacterCreatorData,
      };
    default:
      return store;
  }
};


export default characterCreatorReducer;