import { CHARACTER_CREATOR_REQUEST_SUCCESS } from '../../../../constatnts';
import { titlesData } from '../../characterSheet/mockData';
import * as actions from '../actions';

describe('CharacterCreator Actions', () => {
  it('should properly perform loadCharacterCreatorData action', () => {
    const action = {
      type: CHARACTER_CREATOR_REQUEST_SUCCESS,
      characterCreatorTitles: titlesData.characterCreator_titles,
    };

    expect(actions.loadCharacterCreatorData());
  });
});