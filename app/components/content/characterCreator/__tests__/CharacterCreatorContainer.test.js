import React from 'react';
import { shallow } from 'enzyme';

import CharacterCreatorContainer from '../CharacterCreatorContainer';

describe('<CharacterCreatorContainer />', () => {
  it('should match snapshot', () => {
    const wrapper = shallow(<CharacterCreatorContainer
      characterCreatorTitles={ {} } loadCharacterCreator={ () => undefined }/>);

    expect(wrapper).toMatchSnapshot();
  });
});