import React from 'react';
import { withStyles, Grid, Button } from '@material-ui/core';

const styles = theme => ({
  fullWidthElement: {
    display: 'flex',
  },
  buttonStyles: {
    marginTop: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit,
  },
});

const Skills = ({ classes, resetData, previousStep, nextStep }) => {

  return(
    <Grid
      container
      spacing={8}
      direction="column"
      justify="center"
      alignItems="stretch">
      <Grid item >
        <Button
          className={classes.buttonStyles}
          variant="contained"
          color="secondary"
          onClick={resetData}>
          Reset
        </Button>
        <Button
          className={classes.buttonStyles}
          variant="contained"
          color="primary"
          onClick={previousStep}>
          Previous
        </Button>
        <Button
          className={classes.buttonStyles}
          variant="contained"
          color="primary"
          disabled
          onClick={nextStep}>
          Next
        </Button>
      </Grid>
    </Grid>
  );
};

Skills.propTypes = {
  ...withStyles.propTypes,
};

export default withStyles(styles)(Skills);