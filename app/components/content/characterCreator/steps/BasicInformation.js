import React from 'react';
import { object, func, string } from 'prop-types';
import { withStyles, TextField, Grid, MenuItem, Button } from '@material-ui/core';

const styles = theme => ({
  root: {
    width: '100%',
  },
  textFieldStyle: {
    width: '100%',
    marginTop: theme.spacing.unit * 2,
  },
  buttonStyles: {
    marginTop: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit,
  },
});

const BasicInformation = ({ classes, titles, chosenType, chosenAge, changeTypeOrAge, resetData, nextStep }) => {

  return(
    <Grid container spacing={8}>
      <Grid item xs={12} sm={6} md={3}>
        <TextField
          fullWidth
          id="name"
          label={titles.name}/>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <TextField
          fullWidth
          id="nickname"
          label={titles.nickname} />
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <TextField
          fullWidth
          id="type"
          select
          label={titles.type}
          onChange={changeTypeOrAge('chosenType')}
          value={chosenType}>
          {
            titles.typeOptions.map(current =>(
              <MenuItem key={current} value={current}>
                {current}
              </MenuItem>
            ))
          }
        </TextField>
      </Grid>
      <Grid item xs={12} sm={6} md={3}>
        <TextField
          fullWidth
          id="age"
          select
          label={titles.age}
          onChange={changeTypeOrAge('chosenAge')}
          value={chosenAge}>
          {
            titles.ageOptions.map(current =>(
              <MenuItem key={current} value={current}>
                {current}
              </MenuItem>
            ))
          }
        </TextField>
      </Grid>
      <Button
        className={classes.buttonStyles}
        variant="contained"
        color="secondary"
        onClick={resetData}>
        Reset
      </Button>
      <Button
        className={classes.buttonStyles}
        variant="contained"
        color="primary"
        onClick={nextStep}>
        Next
      </Button>
    </Grid>
  );
};

BasicInformation.propTypes = {
  ...withStyles.propTypes,
  chosenType: string.isRequired,
  changeTypeOrAge: func.isRequired,
  titles: object, //ToDo: change to shape when it will be defined
};


export default withStyles(styles)(BasicInformation);