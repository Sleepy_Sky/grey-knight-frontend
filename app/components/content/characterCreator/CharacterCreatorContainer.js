import React, { Component } from 'react';
import { object, func } from 'prop-types';
import { withStyles, Stepper, Step, StepLabel, StepContent } from '@material-ui/core';
import BasicInformation from './steps/BasicInformation';
import Skills from './steps/Skills';

const styles = () => ({
  root: {
    width: '100%',
  },

});


class CharacterCreatorContainer extends Component {
  state = {
    activeStep: 0,
    chosenType: '',
    chosenAge: '',
  };

  componentDidMount() {
    this.props.loadCharacterCreator();
  }

  nextStep = () => {
    this.setState(state => ({
      activeStep: state.activeStep + 1,
    }));
  };

  previousStep = () => {
    this.setState(state => ({
      activeStep: state.activeStep - 1,
    }));
  };

  resetData = () => {
    this.setState({
      activeStep: 0,
    });
  };

  changeToChosen = name => event => {
    this.setState({
      [name]: event.target.value,
    });
  };

  render() {
    const { classes, characterCreatorTitles } = this.props;
    const { activeStep, chosenType, chosenAge } = this.state;

    const steps = characterCreatorTitles.steps;

    return(
      <div className={classes.root}>
        <Stepper activeStep={activeStep} orientation="vertical">
          <Step>
            <StepLabel>{steps[0]}</StepLabel>
            <StepContent>
              <BasicInformation
                titles={characterCreatorTitles.basicInformation}
                chosenType={chosenType}
                chosenAge={chosenAge}
                changeTypeOrAge={this.changeToChosen}
                nextStep={this.nextStep}
                resetData={this.resetData}/>
            </StepContent>
          </Step>
          <Step>
            <StepLabel>{steps[1]}</StepLabel>
            <StepContent>
              <Skills
                resetData={this.resetData}
                previousStep={this.previousStep}
                nextStep={this.nextStep} />
            </StepContent>
          </Step>

        </Stepper>
      </div>
    );
  }
}

CharacterCreatorContainer.propTypes = {
  ...withStyles.propTypes,
  loadCharacterCreator: func.isRequired,
  characterCreatorTitles: object.isRequired,
};




export default withStyles(styles)(CharacterCreatorContainer);
