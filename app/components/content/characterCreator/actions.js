import { CHARACTER_CREATOR_REQUEST_SUCCESS } from '../../../constatnts';
import { titlesData } from '../characterSheet/mockData';


export const characterCreatorRequestSuccess = () => {
  const toReturn = titlesData.characterCreator_titles;
  return ({
    type: CHARACTER_CREATOR_REQUEST_SUCCESS,
    characterCreatorTitles: toReturn,
  });
};



export const loadCharacterCreatorData = dispatch => () => {
  //  if characterCreatorRequest ? characterCreatorRequest Success : error
  dispatch(characterCreatorRequestSuccess());
};