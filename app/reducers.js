import { combineReducers } from 'redux';
import characterSheetReducer from './components/content/characterSheet/reducer';
import characterCreatorReducer from './components/content/characterCreator/reducer';
import headerReducer from './components/header/reducer';
import globalReducer from './components/reducer';

export default combineReducers({ characterSheetReducer,
  characterCreatorReducer, globalReducer, headerReducer });